import React, {useCallback, useEffect, useRef} from 'react'
import AppNavigation from './navigation/AppNavigator'
import {AppState, Linking, StatusBar, StyleSheet, View} from 'react-native'
import IndicatorDialog from './components/IndicatorDialog'
import {connect, useDispatch, useSelector} from 'react-redux'
import Toast from './components/Toast'
import NetInfo from '@react-native-community/netinfo'
import {setGlobalIndicatorVisibility, setInternetState} from './store/actions/appServices'
import {oneSignalHandlers, setUpOneSignal, sendTags} from './utilities/OneSignal'
import Alert from './components/Alert'
import {checkTokenValid} from './store/actions/authServices'
import {navigate} from './navigation/NavigationService'
import RouteKey from './navigation/RouteKey'
import {getUserLocation} from './utilities/utils'
import {setUserLocation} from './store/actions/user'
import BarCodeScreen from './screens/HomeComponent/BarCodeScreen'
import {setLanguage} from './locale/en'
import {setColors} from './Themes'
import {getData} from './utilities/storage'
import {APP_COLORS, APP_LANGUAGE} from './constants/constants'
import {configuration} from './locale/I18nConfig'
import * as infoServicesActions from './store/actions/infoServices'

function MainLayout({showGlobalIndicator}) {
  const dispatch = useDispatch()
  const profile = useSelector(state => state.infoServices.profile)
  const appState = useSelector(state => state.app.appState)
  const appStateRef = useRef(AppState.currentState)

  const handleCheckNetwork = useCallback(() => {
    NetInfo.addEventListener(state => {
      if ((state.type === 'wifi' || state.type === 'cellular') && state.isConnected) {
        dispatch(setInternetState(true))
      } else {
        dispatch(setInternetState(false))
      }
    })
  }, [dispatch])

  const getAppData = useCallback(async () => {
    try {
      const language = await getData(APP_LANGUAGE)
      if (language) {
        setLanguage(language)
        configuration()
      }
      const colors = await getData(APP_COLORS)
      if (colors) {
        setColors(colors)
      }
    } catch (e) {}
  }, [])

  useEffect(() => {
    getAppData()
    handleCheckNetwork()
    setUpOneSignal()
    oneSignalHandlers()
    getUserLocation(data => {
      dispatch(setUserLocation({...data, permission: 'granted'}))
    })
  }, [dispatch, getAppData, handleCheckNetwork])

  const handleAppState = useCallback(() => {
    AppState.addEventListener('change', nextAppState => {
      if (appStateRef.current.match(/inactive|background/) && nextAppState === 'active') {
        dispatch(infoServicesActions.fetchProfile())
      }
      appStateRef.current = nextAppState
    })
  }, [dispatch])

  const handleValidateToken = useCallback(
    link => {
      try {
        dispatch(
          checkTokenValid(async value => {
            if (value?.id_token) {
              navigate(RouteKey.YourorderScreen, {
                params: {
                  qrcodeUri: link
                }
              })
            }
          })
        )
      } catch (e) {
        console.log(e)
      } finally {
        dispatch(setGlobalIndicatorVisibility(false))
      }
    },
    [dispatch]
  )

  const handleDeepLink = useCallback(() => {
    Linking.getInitialURL().then(res => {
      //This function only work when disable debug mode.
      if (res) {
        handleValidateToken(res)
      }
    })
    Linking.addEventListener('url', res => {
      if (res?.url) {
        handleValidateToken(res?.url)
      }
    })
  }, [handleValidateToken])

  useEffect(() => {
    if (appState === 'Main') {
      handleAppState()
      handleDeepLink()
    }
  }, [appState, handleAppState, handleDeepLink])

  useEffect(() => {
    sendTags({
      email: profile?.email,
      tier_name: profile?.member?.member_tier?.tier?.name,
      tier_id: profile?.member?.member_tier?.tier?.id
    })
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [profile?.email])

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <AppNavigation />
      {showGlobalIndicator && <IndicatorDialog />}
      <Toast />
      <Alert />
      <BarCodeScreen />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default connect(
  state => ({
    showGlobalIndicator: state.app.showGlobalIndicator
  }),
  dispatch => ({})
)(MainLayout)
