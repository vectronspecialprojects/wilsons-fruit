import React, {useEffect, useRef, useState} from 'react'
import {View, Animated, StyleSheet, TextInput, TouchableOpacity, Text, Keyboard} from 'react-native'
import Colors from '../Themes/Colors'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Fonts from '../Themes/Fonts'

export function FloatTitleTextInput({
  value,
  title,
  onChangeText,
  keyboardType,
  messageError,
  secureTextEntry,
  icon,
  onPress,
  style,
  messageInfo,
  countryCodeComponent,
  countryCode,
  onSelectCountry,
  placeholder,
  onSubmitEditing,
  titleStyle,
  isRequire,
  editable = true,
  inputStyle,
  multiline = false,
  rightIcon,
  textInputStyle,
  maxLength,
  refs,
  onFocus,
  onBlur,
  leftIcon,
  floatTitle,
  placeholderTextInput,
  blurOnSubmit,
  onEndEditing,
  backgroundDisabled = Colors().disabled
}) {
  const [isFieldActive, setFieldActive] = useState(false)
  const position = useRef(new Animated.Value(value ? 1 : 0)).current
  const textInputRef = useRef(null)
  const [isFocus, setFocus] = useState(false)
  const [titleWidth, setTitleWidth] = useState(0)

  useEffect(() => {
    if (!isFieldActive && value) {
      setFieldActive(true)
      Animated.timing(position, {
        toValue: 1,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [value])

  const _handleFocus = () => {
    if (!isFieldActive) {
      !refs && textInputRef.current.focus()
      setFieldActive(true)
      Animated.timing(position, {
        toValue: 1,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
  }

  const _handleBlur = () => {
    if (isFieldActive && !value) {
      setFieldActive(false)
      return Animated.timing(position, {
        toValue: 0,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
  }

  return (
    <TouchableOpacity
      onPress={() => {
        Keyboard.dismiss()
        if (onPress) {
          onPress()
        } else {
          _handleFocus()
        }
      }}
      style={[styles.container, style]}
      disabled={!onPress}
    >
      <View style={{flex: 1, flexDirection: 'row'}} pointerEvents={onPress ? 'none' : 'auto'}>
        {!!floatTitle && (
          <>
            <Animated.View
              style={[
                styles.viewHideLine,
                {
                  backgroundColor: Colors().formInputFieldsFill,
                  width: titleWidth,
                  opacity: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1]
                  })
                }
              ]}
            />
            <Animated.Text
              onLayout={e => {
                setTitleWidth(e.nativeEvent.layout.width)
              }}
              pointerEvents={'none'}
              onPress={() => _handleFocus()}
              style={[
                styles.titleStyles,
                {
                  color: Colors().formInputFieldsText
                },
                titleStyle,
                {
                  paddingLeft: countryCodeComponent ? (isIOS() ? 90 : 100) : 0,
                  top: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [responsiveHeight(12), responsiveHeight(-6)]
                  }),
                  fontSize: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [responsiveFont(14), responsiveFont(11)]
                  })
                }
              ]}
            >
              {floatTitle}
            </Animated.Text>
          </>
        )}
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <View
            style={[
              styles.textInputContainer,
              {backgroundColor: Colors().formInputFieldsFill},
              inputStyle,
              messageError && {borderColor: Colors().errorText},
              !editable && {backgroundColor: backgroundDisabled}
            ]}
            pointerEvents={editable ? 'auto' : 'none'}
          >
            {!!countryCodeComponent && countryCodeComponent}
            {!!leftIcon && leftIcon}
            <TextInput
              value={value}
              ref={refs || textInputRef}
              style={[styles.textInput(Colors().formInputFieldsText, countryCode), {}, textInputStyle]}
              onChangeText={text => {
                onChangeText && onChangeText(text)
              }}
              maxLength={maxLength}
              placeholder={placeholderTextInput}
              placeholderTextColor={Colors().formInputFieldsText}
              keyboardType={keyboardType || 'default'}
              underlineColorAndroid="transparent"
              secureTextEntry={secureTextEntry}
              onEndEditing={onEndEditing}
              blurOnSubmit={blurOnSubmit}
              onSubmitEditing={() => {
                if (onSubmitEditing) {
                  onSubmitEditing()
                }
                setFocus(false)
              }}
              onBlur={() => {
                setFocus(false)
                _handleBlur()
                !!onBlur && onBlur()
              }}
              autoCapitalize="none"
              editable={editable && !onPress}
              scrollEnabled={false}
              multiline={multiline}
              textAlignVertical={'top'}
              onFocus={() => {
                _handleFocus()
                setFocus(true)
                !!onFocus && onFocus()
              }}
            />
            {isFocus && value?.length > 0 && (
              <TouchableOpacity onPress={() => onChangeText('')} style={{marginRight: 10}}>
                <MaterialCommunityIcons
                  name={'close-circle'}
                  color={Colors().formInputFieldsText}
                  size={20}
                />
              </TouchableOpacity>
            )}
            {!!rightIcon && rightIcon}
          </View>
          {!!messageError && (
            <Text style={styles.errorMessage(Colors().errorText)}>{messageError || messageInfo}</Text>
          )}
          {!!messageInfo && !messageError && <Text style={styles.messageInfo}>{messageInfo}</Text>}
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    minHeight: 40,
    marginBottom: responsiveHeight(10),
    marginTop: responsiveHeight(5)
  },
  titleStyles: {
    position: 'absolute',
    fontSize: responsiveFont(14),
    left: responsiveWidth(20),
    zIndex: 999,
    fontFamily: Fonts.regular
  },
  textInputContainer: {
    paddingLeft: responsiveWidth(10),
    height: responsiveHeight(48),
    minHeight: 38,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: responsiveWidth(24)
  },
  textInput: (color, countryCode) => ({
    flex: 1,
    textAlignVertical: 'center',
    fontSize: responsiveFont(16),
    marginBottom: 0,
    paddingVertical: 0,
    fontFamily: Fonts.regular,
    paddingLeft: countryCode ? 15 : 0,
    color
  }),
  viewHideLine: {
    height: 4,
    position: 'absolute',
    left: responsiveWidth(20),
    zIndex: 999,
    top: -1
  },
  messageInfo: {
    fontSize: responsiveHeight(11),
    marginTop: responsiveHeight(3),
    fontFamily: Fonts.regular
  },
  errorMessage: color => ({
    fontSize: responsiveHeight(11),
    marginTop: responsiveHeight(3),
    fontFamily: Fonts.regular,
    color
  })
})
