import React, {useMemo} from 'react'
import {View, StyleSheet} from 'react-native'
import Styles from '../Themes/Styles'
import {isIOS, responsiveHeight, responsiveWidth, shadow} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import FastImage from 'react-native-fast-image'
import {useSelector} from 'react-redux'
import {defaultDistance} from '../constants/constants'
import Images from '../Themes/Images'
import Colors from '../Themes/Colors'
import ComponentContainer from './ComponentContainer'
import {CardText} from './Text'
import {isTrue} from '../utilities/utils'

const AddressCard = props => {
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  const appFlags = useSelector(state => state.app.appFlags)
  const {imgSource, distance, venueAddress} = props
  const address = useMemo(() => {
    const after = venueAddress?.slice(venueAddress.indexOf(',') + 1)?.trim()
    const before = venueAddress?.slice(0, venueAddress.indexOf(','))?.trim()
    return {
      after,
      before
    }
  }, [venueAddress])
  return (
    <TouchableCmp onPress={props.onPress}>
      <View style={isTrue(appFlags.app_is_component_shadowed) && shadow}>
        <ComponentContainer
          style={[
            styles.container,
            !useLegacyDesign && {
              ...styles.newDesign
            },
            {backgroundColor: Colors().cardFill},
            !isIOS() && isTrue(appFlags.app_is_component_shadowed) && shadow
          ]}
        >
          <FastImage
            source={imgSource ? {uri: imgSource} : Images.defaultImage}
            style={styles.image}
            resizeMode={'contain'}
          />
          <View style={styles.textWrapper}>
            <CardText style={{...Styles.mediumCapText, alignSelf: 'flex-start'}}>{props.venueName}</CardText>
            <CardText style={[Styles.xSmallNormalText, styles.address]}>{address?.before}</CardText>
            <CardText
              style={[
                Styles.xSmallNormalText,
                styles.address,
                {
                  marginTop: 0
                }
              ]}
            >
              {address?.after}
            </CardText>
            {distance !== defaultDistance && (
              <CardText
                style={{
                  ...Styles.xSmallNormalText,
                  ...styles.address,
                  marginTop: 0
                }}
              >
                {distance?.toFixed(2)} Km
              </CardText>
            )}
          </View>
        </ComponentContainer>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(12),
    alignItems: 'center'
  },
  image: {
    width: responsiveWidth(100),
    aspectRatio: 1
  },
  textWrapper: {
    flex: 1,
    height: '100%',
    paddingVertical: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(15)
  },
  address: {
    textAlign: 'left',
    marginTop: responsiveHeight(10),
    lineHeight: responsiveHeight(20)
  },
  newDesign: {
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(15),
    overflow: 'hidden'
  }
})

export default AddressCard
