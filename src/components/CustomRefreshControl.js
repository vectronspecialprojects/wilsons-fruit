import React from 'react'
import {RefreshControl} from 'react-native'
import Colors from '../Themes/Colors'
import {localize} from '../locale/I18nConfig'

function CustomRefreshControl({refreshing, onRefresh, ...rest}) {
  return (
    <RefreshControl
      {...rest}
      refreshing={refreshing}
      onRefresh={onRefresh}
      tintColor={Colors().linkText}
      titleColor={Colors().linkText}
      title={localize('pullToRefresh')}
    />
  )
}

export default CustomRefreshControl
