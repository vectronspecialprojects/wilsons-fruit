import React from 'react'
import {View, StyleSheet} from 'react-native'
import {shadow} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import {TouchableCmp} from './UtilityFunctions'

const CardContainer = ({style, isShadow, children, onPress, disabled, ...rest}) => {
  return (
    <TouchableCmp disabled={!onPress || disabled} onPress={onPress} {...rest}>
      <View style={[styles.container(Colors().cardFill, isShadow), style]}>{children}</View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: (backgroundColor, isShadow) => {
    let style = {}
    if (isShadow) {
      style = {
        ...style,
        ...shadow
      }
    }
    return {
      backgroundColor,
      ...style
    }
  }
})

export default CardContainer
