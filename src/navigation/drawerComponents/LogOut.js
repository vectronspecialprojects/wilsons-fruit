import React from 'react'
import {View, TouchableOpacity, StyleSheet} from 'react-native'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import Colors from '../../Themes/Colors'
import Fonts from '../../Themes/Fonts'
import {CardText} from '../../components/Text'

const LogOut = props => (
  <TouchableOpacity onPress={props.onPress} style={styles.logoutTouch}>
    <View style={styles.logoutContainer}>
      <FontAwesome5Pro name="sign-out-alt" size={22} style={{width: 30}} color={Colors().cardText} />
      <CardText style={styles.logoutText}>Logout</CardText>
    </View>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  logoutTouch: {
    width: '100%',
    marginTop: 15,
    marginBottom: 150
  },
  logoutContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 20
  },
  logoutText: {
    fontFamily: Fonts.openSansBold,
    fontSize: 16,
    paddingLeft: 30
  }
})

export default LogOut
