import React, {useMemo, useState} from 'react'
import {ScrollView, StyleSheet} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import TextInputView from '../../components/TextInputView'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {changeEmail} from '../../store/actions/authServices'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'
import RouteKey from '../../navigation/RouteKey'
import ScreenContainer from '../../components/ScreenContainer'

const ChangeEmailScreen = props => {
  const [newMail, setNewMail] = useState('')
  const [confirmMail, setConfirmMail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const profile = useSelector(state => state.infoServices.profile)
  const emailNotAllowedDomains = useSelector(state => state.app.emailNotAllowedDomains)
  const {autoFill} = props.route?.params || {}

  async function handleChangeEmail() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      await dispatch(changeEmail(newMail.value, password.value))
      props.navigation.navigate(RouteKey.Home)
      Toast.success(localize('changeEmail.messageSuccess'))
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const disableButton = useMemo(() => {
    return !(
      confirmMail?.value &&
      newMail?.value &&
      password?.value &&
      newMail?.isValid &&
      confirmMail?.isValid &&
      password?.isValid
    )
  }, [confirmMail, newMail, password])

  function handleChangeValue(id, value, isValid) {
    switch (id) {
      case 'email':
        setNewMail({value, isValid})
        break
      case 'confirmEmail':
        setConfirmMail({value, isValid})
        break
      case 'password':
        setPassword({value, isValid})
    }
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('changeEmail.title')} />
      <ScrollView style={styles.container} bounces={false}>
        {autoFill && (
          <TextInputView
            floatTitle={localize('changeEmail.currentMail')}
            value={profile.email}
            editable={false}
          />
        )}
        <TextInputView
          floatTitle={localize('changeEmail.newEmail')}
          value={newMail?.value}
          onChangeText={handleChangeValue}
          emailAvailability={true}
          email
          messageError={localize('changeEmail.invalidEmail')}
          emailNotAllowedDomains={emailNotAllowedDomains}
          id={'email'}
        />
        <TextInputView
          floatTitle={localize('changeEmail.confirmNewEmail')}
          value={confirmMail.value}
          onChangeText={handleChangeValue}
          id={'confirmEmail'}
          email
          messageError={localize('changeEmail.confirmEmailNotMatch')}
          crossCheckPassword={newMail?.value}
          confirmPassword
        />
        <TextInputView
          floatTitle={localize('changeEmail.password')}
          secureTextEntry={true}
          value={password.value}
          onChangeText={handleChangeValue}
          messageError={localize('signUp.errPassword')}
          id={'password'}
        />
        <PositiveButton
          title={localize('changeEmail.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangeEmail()}
        />
        <NegativeButton
          title={localize('changeEmail.cancel')}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20)
  }
})

export default ChangeEmailScreen
