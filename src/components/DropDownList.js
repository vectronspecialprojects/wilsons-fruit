import React, {useMemo, useState} from 'react'
import {Text, StyleSheet, View, TouchableOpacity, FlatList, Modal, KeyboardAvoidingView} from 'react-native'
import TextInputView from './TextInputView'
import {
  deviceHeight,
  hitSlop,
  isIOS,
  responsiveFont,
  responsiveHeight,
  responsiveWidth
} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {Picker} from '@react-native-picker/picker'
import SeparatorComponent from './SeparatorComponent'
import ComponentContainer from './ComponentContainer'
import {CardText, Link} from './Text'

export default function DropDownList({
  visible,
  onClose,
  data,
  hasSearchBar,
  onSelect,
  value,
  modalTitle,
  style,
  textStyle,
  height = deviceHeight(),
  showValue,
  listEmptyText = '',
  children,
  placeholder
}) {
  const [keyWord, setKeyword] = useState('')

  const filterList = useMemo(() => {
    if (!keyWord) {
      return data
    } else {
      let listGroup = []
      const formatKeyword = keyWord.trim().toLowerCase()
      listGroup = data.filter(item => {
        const selected = formatShowValue(item)
        return selected.toLowerCase().includes(formatKeyword)
      })
      return listGroup
    }
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [keyWord, data])

  function formatShowValue(item) {
    if (typeof showValue === 'string') {
      return item[showValue]
    } else if (Array.isArray(showValue)) {
      return showValue.map(i => item[i]).join(', ')
    } else {
      return item
    }
  }

  const renderItem = ({item, index}) => {
    const selected = formatShowValue(item)

    return (
      <TouchableOpacity
        key={item.value}
        style={[styles.itemContainer, {backgroundColor: Colors().backgroundFill}]}
        onPress={() => {
          onSelect(item, true)
          onClose()
          setKeyword('')
        }}
      >
        <CardText
          color={Colors().backgroundText}
          style={{
            fontFamily: value === selected ? Fonts.openSansBold : Fonts.openSans,
            fontSize: responsiveFont(15),
            marginVertical: 2,
            flex: 1
          }}
        >
          {selected}
        </CardText>
        {value === selected && <MaterialIcons name={'check'} size={22} color={'#62CAA4'} />}
      </TouchableOpacity>
    )
  }

  function renderListForIOS() {
    return (
      <View>
        <Picker
          selectedValue={value}
          onValueChange={(itemValue, itemIndex) => {
            if (itemIndex === 0) {
              onSelect(null, false)
            } else {
              onSelect(filterList[itemIndex - 1], true)
            }
          }}
        >
          {[{[showValue]: placeholder, value: 'none'}, ...filterList]?.map(item => {
            const selected = formatShowValue(item)
            return (
              <Picker.Item
                label={selected}
                value={item.value}
                key={item.value}
                color={Colors().backgroundText}
              />
            )
          })}
        </Picker>
      </View>
    )
  }

  function renderListForAndroid() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        bounces={false}
        keyExtractor={(item, index) => index.toString()}
        keyboardShouldPersistTaps={'handled'}
        data={filterList}
        renderItem={renderItem}
        ItemSeparatorComponent={() => <SeparatorComponent />}
        ListEmptyComponent={
          <Text
            style={{
              fontSize: responsiveFont(14),
              marginTop: responsiveHeight(20),
              fontFamily: Fonts.openSans,
              color: Colors().backgroundText
            }}
          >
            {listEmptyText}
          </Text>
        }
      />
    )
  }

  return (
    <Modal visible={visible} animationType={'slide'} transparent={true} onRequestClose={onClose}>
      <TouchableOpacity
        onPress={() => {
          // onClose()
          // setKeyword('')
        }}
        style={styles.container}
        activeOpacity={1}
      >
        <KeyboardAvoidingView
          style={{flex: 1, justifyContent: 'flex-end'}}
          behavior={isIOS() ? 'padding' : null}
        >
          <ComponentContainer style={{...styles.wrapper, height}}>
            {modalTitle && (
              <ComponentContainer style={styles.titleWrapper}>
                {!isIOS() && (
                  <TouchableOpacity
                    hitSlop={hitSlop}
                    onPress={() => {
                      setKeyword('')
                      onClose()
                    }}
                    style={{position: 'absolute', zIndex: 999}}
                  >
                    <MaterialIcons name={'close'} size={30} color={Colors().gray} />
                  </TouchableOpacity>
                )}
                <CardText style={styles.textSemi} color={Colors().backgroundText}>
                  {modalTitle}
                </CardText>
                {isIOS() && (
                  <TouchableOpacity
                    hitSlop={hitSlop}
                    onPress={() => {
                      setKeyword('')
                      onClose()
                    }}
                    style={{position: 'absolute', zIndex: 999, right: 0}}
                  >
                    <Link
                      style={{
                        fontFamily: Fonts.bold,
                        fontSize: responsiveFont(15)
                      }}
                    >
                      Done
                    </Link>
                  </TouchableOpacity>
                )}
              </ComponentContainer>
            )}
            {hasSearchBar && (
              <TextInputView
                placeholder={'Search'}
                inputStyle={{
                  borderWidth: StyleSheet.hairlineWidth,
                  borderRadius: 20
                }}
                value={keyWord}
                onChangeText={text => setKeyword(text)}
                rightIcon={<MaterialIcons name={'search'} size={20} color={Colors().formInputFieldsText} />}
                textInputStyle={{
                  fontFamily: Fonts.openSans
                }}
              />
            )}
            {children ? children : isIOS() ? renderListForIOS() : renderListForAndroid()}
          </ComponentContainer>
        </KeyboardAvoidingView>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  wrapper: {
    // paddingBottom: responsiveHeight(25),
    borderTopLeftRadius: responsiveWidth(20),
    borderTopRightRadius: responsiveWidth(20),
    paddingHorizontal: responsiveWidth(15),
    borderRadius: 5
  },
  titleWrapper: {
    paddingVertical: responsiveHeight(10),
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: responsiveHeight(10),
    flexDirection: 'row'
  },
  textSemi: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    flex: 1,
    fontFamily: Fonts.openSans
  },
  itemContainer: {
    minHeight: 40,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(10)
  }
})
