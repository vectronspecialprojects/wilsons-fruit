import React from 'react'
import {StyleSheet, TouchableOpacity, FlatList} from 'react-native'
import Colors from '../../Themes/Colors'
import {deviceWidth, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'
import FastImage from 'react-native-fast-image'
import Images from '../../Themes/Images'
import ScreenContainer from '../../components/ScreenContainer'

function GamingScreen({navigation}) {
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.item(Colors().cardFill)}
        onPress={() => {
          navigation.navigate(item.route)
        }}
      >
        <FastImage
          source={item.image}
          style={styles.icon}
          tintColor={Colors().white}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    )
  }
  return (
    <ScreenContainer style={styles.container}>
      <FlatList
        data={[
          {route: RouteKey.WheelGameScreen, image: Images.wheelGame},
          {
            route: RouteKey.ScratchGameScreen,
            image: Images.scratchGame
          }
        ]}
        style={{marginTop: responsiveHeight(50)}}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24)
  },
  item: backgroundColor => ({
    marginRight: responsiveWidth(24),
    width: (deviceWidth() - responsiveWidth(72)) / 2,
    aspectRatio: 1,
    borderRadius: responsiveWidth(12),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor
  }),
  icon: {
    width: responsiveWidth(90),
    height: responsiveWidth(90)
  }
})

export default GamingScreen
