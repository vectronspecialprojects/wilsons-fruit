import React, {useRef} from 'react'
import {Modal, TouchableOpacity, StyleSheet, View, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {deviceWidth, responsiveFont, responsiveHeight} from '../../../Themes/Metrics'
import {NegativeButton, PositiveButton} from '../../../components/ButtonView'
import Fonts from '../../../Themes/Fonts'
import {useDispatch, useSelector} from 'react-redux'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import {localize} from '../../../locale/I18nConfig'
import {resendMail} from '../../../utilities/ApiManage'
import Toast from '../../../components/Toast'
import * as infoServicesActions from '../../../store/actions/infoServices'

function ModalConfirmEmail({visible, onClose}) {
  const emailPendingMessage = useSelector(state => state.app.emailPendingMessage)
  const profile = useSelector(state => state.infoServices.profile)
  const debounce = useRef()
  const dispatch = useDispatch()

  async function handleResendEmail() {
    try {
      const res = await resendMail(profile?.member?.id)
      if (!res.ok) {
        throw new Error(res.message)
      }
      Toast.info(res.message)
    } catch (e) {
      Toast.info(e.message)
    }
  }

  function handleUserClick() {
    clearTimeout(debounce.current)
    debounce.current = setTimeout(() => {
      dispatch(infoServicesActions.fetchProfile())
    }, 1000)
  }

  return (
    <Modal visible={visible} onRequestClose={handleUserClick} transparent>
      <TouchableOpacity activeOpacity={1} onPress={handleUserClick} style={styles.container}>
        <TouchableOpacity style={styles.wrapper(Colors().alertDialogFill)} activeOpacity={1}>
          <Text style={[styles.title, {color: Colors().alertDialogText}]}>Account Pending Verification</Text>
          <Text style={[styles.message, {color: Colors().alertDialogText}]}>{emailPendingMessage}</Text>
          <View style={{flexDirection: 'row', marginTop: responsiveHeight(15)}}>
            <NegativeButton
              title={localize('changeEmail.title')}
              style={styles.changeEmailButton}
              onPress={() => {
                navigate(RouteKey.ChangeEmailScreen, {
                  autoFill: true
                })
                onClose?.()
              }}
            />
            <PositiveButton title={'Resend'} style={{flex: 1}} onPress={() => handleResendEmail()} />
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
      <Toast />
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapper: backgroundColor => ({
    backgroundColor,
    width: deviceWidth() - 40,
    borderRadius: responsiveHeight(12),
    padding: responsiveHeight(12)
  }),
  title: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    textAlign: 'center'
  },
  message: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans,
    marginVertical: responsiveHeight(10),
    textAlign: 'center'
  },
  changeEmailButton: {
    flex: 1,
    marginRight: responsiveHeight(10),
    backgroundColor: 'transparent',
    borderWidth: 1
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 9999
  }
})

export default ModalConfirmEmail
