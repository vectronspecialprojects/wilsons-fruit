import React from 'react'
import {TouchableOpacity} from 'react-native'
import {responsiveFont} from '../Themes/Metrics'
import VectorIconButton from './VectorIconButton'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {CardText} from './Text'

export default function ({title, onPress, value, size = 30, style, textStyle, outerColor, innerColor}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center'
        },
        style
      ]}
    >
      <VectorIconButton
        Component={MaterialIcons}
        name={value ? 'radio-button-checked' : 'radio-button-unchecked'}
        size={size}
        color={value ? innerColor : outerColor}
        disabled={true}
      />
      <CardText
        style={[
          {
            fontSize: responsiveFont(13),
            flex: 1
          },
          textStyle
        ]}
      >
        {title}
      </CardText>
    </TouchableOpacity>
  )
}
