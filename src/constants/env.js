import {Platform} from 'react-native'

const env = {
  production: 'production',
  staging: 'staging'
}

const appSecret = '2da489f7ac6cce34ddcda02ef126270b'
const appToken = '12a3e1a732ee'

export const buildEvn = env.production

const baseUrl = {
  staging: 'https://api2.vecport.net/mpwilsonsfruit/api/',
  production: 'https://api2.vecport.net/mpwilsonsfruit/api/'
}

export const codePushKey = Platform.select({
  ios: {
    staging: '_Fd3KJvTCdY1a3A1kTnbIxe8T7B6tNu0gEV7n',
    production: 'Rn1b_UYH1UHrk23bfejf1-8PJ-L5Z6dvkTIzY',
  },
  android: {
    staging: 'csYbbUHlkg7dpWYpV6mOSQq_ZZxRG3BgGIRIu',
    production: 'YY-r9qu3gsf3MC4pXKuuPlqbNTdgzuC95yogK',
  },
})

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: 'ab343771-fdab-474b-b0d9-a2a65c1c1a97',
  buildEvn: buildEvn,
  codePushKey: codePushKey[buildEvn],
  baseUrl: baseUrl[buildEvn],
  appSecret,
  appToken
}

export default vars
