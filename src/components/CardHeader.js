import React from 'react'
import {View} from 'react-native'
import Styles from '../Themes/Styles'
import {CardText} from './Text'

const CardHeader = props => {
  return (
    <View style={{...Styles.centerContainer, ...props.style}}>
      <CardText style={[Styles.header, props.titleStyle]}>{props.title}</CardText>
    </View>
  )
}

export default CardHeader
