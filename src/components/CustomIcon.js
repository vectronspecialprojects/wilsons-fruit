/**
 * Created by Hong HP on 9/20/21.
 */
import React from 'react'
import {Image} from 'react-native'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'

function CustomIcon({name, image_icon, color, size, style, tintColor, icon_selector}) {
  if (icon_selector === 'image' && !image_icon.includes('https://via.placeholder.com')) {
    return (
      <Image style={[style, {width: size, height: size, tintColor: tintColor}]} source={{uri: image_icon}} />
    )
  }
  if (name) {
    return <FontAwesome5Pro name={name} style={style} size={size} color={color} />
  }
  return null
}

export default CustomIcon
