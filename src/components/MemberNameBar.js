import React from 'react'
import {View, StyleSheet} from 'react-native'
import {FontAwesome5Touch} from './UtilityFunctions'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {responsiveFont} from '../Themes/Metrics'
import {CardText} from './Text'

const MemberNameBar = props => {
  return (
    <View style={{...styles.nameBar, ...props.style}}>
      <CardText style={{...Styles.largeCapText, ...{paddingRight: 5}}}>
        {props.name?.member?.first_name} {props.name?.member?.last_name}
      </CardText>
      <FontAwesome5Touch
        name="edit"
        size={responsiveFont(15)}
        color={Colors().cardText}
        onPress={props.onIconPress}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  nameBar: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  }
})

export default MemberNameBar
