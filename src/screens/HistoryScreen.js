import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {FlatList} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import HistoryItem from './../components/HistoryItem'
import DateFilterComponent from './../components/DateFilterComponent'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'
import CustomRefreshControl from '../components/CustomRefreshControl'

const HistoryScreen = props => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const transactions = useSelector(state => state.infoServices.transactions)
  const [filter, setFilter] = useState({})

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.getTransaction())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function renderItem({item, index}) {
    return <HistoryItem data={item} />
  }

  const dataShow = useMemo(() => {
    if (!filter?.from && !filter?.to) {
      return transactions
    }
    return transactions
      .filter(item => item.ordered_at >= filter?.from)
      .filter(item => (filter?.to ? item.ordered_at <= filter?.to : true))
  }, [filter, transactions])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('history')} />
      <DateFilterComponent
        onChange={data => {
          setFilter(data)
        }}
      />
      <FlatList
        data={dataShow}
        renderItem={renderItem}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
        keyExtractor={(item, index) => index.toString()}
      />
    </ScreenContainer>
  )
}

export default HistoryScreen
