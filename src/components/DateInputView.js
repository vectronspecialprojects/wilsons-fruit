import React, {useEffect, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight} from '../Themes/Metrics'
import DateTime from './DateTime'
import Fonts from '../Themes/Fonts'
import dayjs from 'dayjs'

function DateInputView({
  onInputChange,
  id,
  style,
  styleText,
  maxDate,
  minDate,
  containerStyle,
  required,
  label,
  errorText,
  initialDate,
  editable
}) {
  const [isValid, setIsValid] = useState(true)
  const [date, setDate] = useState(initialDate || '')

  const dateChangeHandler = data => {
    let valid
    if (required && !data) {
      valid = false
    } else if (dayjs().diff(data, 'years') >= 18) {
      valid = true
    } else {
      valid = false
    }
    setIsValid(valid)
    onInputChange?.(id, dayjs(data).format('DD/MM/YYYY'), valid)
    setDate(data)
  }

  useEffect(() => {
    if (!!initialDate && editable) {
      dateChangeHandler(initialDate)
    }
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [])

  return (
    <View style={[containerStyle]}>
      <DateTime
        pickerStyle={[styles.pickerStyle, style]}
        value={date}
        onChange={dateChangeHandler}
        maxDate={maxDate}
        minDate={minDate}
        title={label}
        isInput={true}
        editable={editable}
      />
      {!isValid && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorText}</Text>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  errorContainer: {
    marginBottom: responsiveHeight(5)
  },
  errorText: {
    fontFamily: Fonts.openSans,
    color: 'red',
    fontSize: responsiveFont(11)
  }
})
export default DateInputView
