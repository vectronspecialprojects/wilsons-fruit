import React from 'react'
import {View, StyleSheet, Image} from 'react-native'
import {responsiveHeight, responsiveWidth, deviceWidth} from '../Themes/Metrics'
import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import {DialogText} from './Text'
import {DialogComponent} from './ComponentContainer'
import {NegativeButton, PositiveButton} from './ButtonView'

const OffersScreenPopup = props => {
  if (props.renderItem) {
    return (
      <Modal isVisible={props.isVisible} animationIn="swing" animationOut="zoomOut">
        <DialogComponent style={styles.popupContainer}>
          <View style={{width: '100%', aspectRatio: 1}}>
            <Image style={styles.image} source={{uri: props.renderItem.image_square}} />
          </View>

          <View style={{padding: responsiveWidth(20)}}>
            <DialogText style={Styles.mediumCapBoldText}>{props.renderItem.heading}</DialogText>
            {props?.renderItem?.products?.length !== 0 && (
              <DialogText
                style={{
                  ...Styles.mediumCapBoldText,
                  ...{
                    marginBottom: responsiveHeight(20),
                    marginTop: responsiveHeight(5)
                  }
                }}
              >
                {+props.renderItem?.products?.[0]?.product?.point_price} Points
              </DialogText>
            )}
            <DialogText style={Styles.xSmallCapText}>{props.renderItem.desc_short}</DialogText>
          </View>

          {props?.renderItem?.products?.length !== 0 ? (
            <View style={styles.popupButtonsBar}>
              <NegativeButton style={styles.button} onPress={props.onCancelPress} title={'Cancel'} />
              <PositiveButton style={styles.button} onPress={props.onOkPress} title={'Redeem'} />
            </View>
          ) : (
            <View style={styles.popupButtonsBar}>
              <NegativeButton style={styles.buttonClose} onPress={props.onCancelPress} title={'Close'} />
            </View>
          )}
        </DialogComponent>
      </Modal>
    )
  }
  return null
}

const styles = StyleSheet.create({
  popupContainer: {
    alignSelf: 'center',
    width: deviceWidth() * 0.7,
    borderRadius: 15,
    overflow: 'hidden'
  },
  popupButtonsBar: {
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(50),
    justifyContent: 'space-between',
    paddingBottom: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(5)
  },
  button: {
    flex: 1,
    maxWidth: '49.5%'
  },
  buttonClose: {
    flex: 1
  },
  buttonTouch: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover'
  }
})

export default OffersScreenPopup
