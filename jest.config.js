const config = {
  verbose: true,
  preset: 'react-native',
  collectCoverageFrom: ['**/*.{js,jsx}', '!**/node_modules/**', '!**/vendor/**'],
  testPathIgnorePatterns: ['<rootDir>/node_modules/'],
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest'
  },
  transformIgnorePatterns: [
    'node_modules/(?!(jest-)?react-native|@react-native-community/react-native|@react-native|@react-navigation|react-native-svg|react-native-geolocation-service|autolinker.*)'
  ],
  setupFiles: ['<rootDir>/__mocks__', '<rootDir>/node_modules/react-native-gesture-handler/jestSetup.js'],
  collectCoverage: true
}

module.exports = config
