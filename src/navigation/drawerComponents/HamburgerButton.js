import React from 'react'
import {useSelector} from 'react-redux'
import {isIOS} from '../../Themes/Metrics'
import HeaderLeftButton from '../../components/HeaderLeftButton'

function HamburgerButton({position, navData}) {
  const sideMenuPosition = useSelector(state => state.app.sideMenuPosition)
  if (position !== sideMenuPosition) {
    return null
  }
  return (
    <HeaderLeftButton
      navData={navData}
      iconName={!isIOS() ? 'md-menu' : 'ios-menu'}
      onPress={() => {
        navData.navigation.toggleDrawer()
      }}
    />
  )
}

export default HamburgerButton
