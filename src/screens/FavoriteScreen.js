import React, {useCallback, useEffect, useState} from 'react'
import {View, StyleSheet, FlatList, ScrollView} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import FavoriteItem from './../components/FavoriteItem'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import RouteKey from '../navigation/RouteKey'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'
import {HeroText} from '../components/Text'
import CustomRefreshControl from '../components/CustomRefreshControl'

const FavoriteScreen = ({navigation}) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const favorites = useSelector(state => state.infoServices.favorites)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.getFavorite())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function renderItem({item, index}) {
    return (
      <FavoriteItem
        data={item?.listing}
        onPress={() => {
          navigation.navigate(RouteKey.FavoriteDetailScreen, {
            favoriteDetail: item?.listing,
            title: item?.listing?.type?.name
          })
        }}
      />
    )
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('favorite.title')} />
      <View style={styles.sessionContainer}>
        <ScrollView
          refreshControl={
            <CustomRefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
            />
          }
        >
          {favorites?.map((item, index) => {
            return (
              <View key={index.toString()}>
                <HeroText style={styles.title}>{item.title}</HeroText>
                <FlatList
                  data={item.data}
                  showsHorizontalScrollIndicator={false}
                  renderItem={renderItem}
                  horizontal={true}
                  keyExtractor={(_, i) => i.toString()}
                />
              </View>
            )
          })}
        </ScrollView>
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: responsiveFont(15),
    marginBottom: responsiveHeight(18),
    fontFamily: Fonts.openSansBold
  },
  sessionContainer: {
    marginLeft: responsiveWidth(17),
    marginTop: responsiveHeight(20)
  }
})

export default FavoriteScreen
