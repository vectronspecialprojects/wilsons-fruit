import React from 'react'
import {View, StyleSheet, Image} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import Images from '../Themes/Images'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {SafeAreaView} from 'react-native-safe-area-context'
import {logoImage} from '../constants/constants'
import ComponentContainer from '../components/ComponentContainer'
import {CardText} from '../components/Text'

function MaintenanceScreen() {
  return (
    <ComponentContainer style={styles.container}>
      <SafeAreaView style={styles.logoContainer}>
        <Image style={styles.logo} source={{uri: logoImage}} />
      </SafeAreaView>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <Image source={Images.maintenance} style={[styles.image, {tintColor: Colors().cardText}]} />
        <CardText style={styles.content}>We are currently undergoing maintenance.</CardText>
      </View>
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(50)
  },
  image: {
    width: responsiveWidth(200),
    height: responsiveHeight(200)
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain'
  },
  logoContainer: {
    alignItems: 'center'
  }
})

export default MaintenanceScreen
