import React, {useMemo, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import QuantityComponent from './QuantityComponent'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import ComponentContainer from '../../../components/ComponentContainer'
import {CardText} from '../../../components/Text'

function ProductItem({data, onChangeQuantity, isRedeem}) {
  const {product} = data

  const [quantity, setQuantity] = useState(data.qty || (data?.listingTypeId === 10 ? 1 : 0))

  const total = useMemo(() => {
    if (isRedeem) {
      return +product?.point_price * quantity || 0
    }
    return +product?.unit_price * quantity || 0
  }, [isRedeem, product, quantity])

  return (
    <ComponentContainer style={styles.container}>
      <View style={{flex: 2}}>
        <Text style={styles.name}>{product?.name}</Text>
        {!product?.is_free && (
          <CardText style={styles.price}>
            {!isRedeem && '$'}
            {isRedeem ? +product?.point_price : product?.unit_price}
            {isRedeem && ' Points'}
          </CardText>
        )}
        {!!product?.is_free && <CardText style={styles.price}>free</CardText>}
      </View>
      <QuantityComponent
        disabled={data?.listingTypeId === 10}
        setValue={value => {
          onChangeQuantity(value)
          setQuantity(value)
        }}
        value={quantity}
      />
      {!product?.is_free && (
        <CardText style={[styles.name, {flex: 1, textAlign: 'right'}]}>
          {!isRedeem && '$'}
          {total.toFixed(isRedeem ? 0 : 2)}
          {isRedeem && ' Points'}
        </CardText>
      )}
      {!!product?.is_free && <CardText style={[styles.name, {flex: 1, textAlign: 'right'}]}>free</CardText>}
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: responsiveWidth(14),
    marginTop: responsiveHeight(16),
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(10),
    alignItems: 'flex-end'
  },
  name: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold
  },
  price: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans
  }
})

export default ProductItem
