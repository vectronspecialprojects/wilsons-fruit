import React, {useRef, useState} from 'react'
import {Modal, StyleSheet, View, Text, Image, Platform} from 'react-native'
import ScreenContainer from '../../../components/ScreenContainer'
import {updateProfileAvatar} from '../../../store/actions/infoServices'
import Avatar from '../../../components/Avatar'
import Colors from '../../../Themes/Colors'
import {HeroComponent} from '../../../components/ComponentContainer'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {useDispatch, useSelector} from 'react-redux'
import {logoImage} from '../../../constants/constants'
import Toast from '../../../components/Toast'
import {isTrue} from '../../../utilities/utils'

function ModalUploadProfile({visible, setShowImageUpload, profile}) {
  const [updateImage, setUpdateImage] = useState('')
  const appFlags = useSelector(state => state.app.appFlags)
  const avatarRef = useRef()
  const dispatch = useDispatch()
  const {first_name, last_name, upload_message} = profile?.member || {}

  async function handleUpdateAvatar(image) {
    try {
      await dispatch(updateProfileAvatar(image))
      setShowImageUpload(false)
      setUpdateImage('')
    } catch (e) {}
  }

  return (
    <Modal visible={visible} onRequestClose={() => {}} transparent>
      <ScreenContainer>
        <HeroComponent style={styles.logoContainer}>
          <Image style={styles.logo} source={{uri: logoImage}} />
        </HeroComponent>
        <View style={{height: responsiveHeight(56), backgroundColor: Colors().heroFill}}>
          <Avatar
            uri={updateImage?.path || profile?.member?.profile_img}
            size={responsiveWidth(112)}
            style={styles.avatar}
            editable={true}
            onImageSelect={image => {
              setUpdateImage(image)
              handleUpdateAvatar(image)
            }}
            ref={avatarRef}
            showSpinner={isTrue(appFlags?.allowed_member_photo_spin)}
          />
        </View>
        <View style={styles.wrapper(Colors().alertDialogFill)} activeOpacity={1}>
          <Text
            style={[styles.message, {color: Colors().alertDialogText}]}
          >{`Hello ${first_name} ${last_name}\n\n${upload_message}`}</Text>
        </View>
      </ScreenContainer>
      <Toast />
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-56),
    position: 'absolute',
    zIndex: 999
  },
  logoContainer: {
    height: Platform.OS === 'ios' ? 100 : 60,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 10
  },
  logo: {
    width: 100,
    height: 50,
    resizeMode: 'contain'
  },
  wrapper: backgroundColor => ({
    backgroundColor,
    width: deviceWidth() - 40,
    borderRadius: responsiveHeight(12),
    padding: responsiveHeight(12),
    alignSelf: 'center',
    marginTop: 150,
    shadowColor: '#000',
    shadowRadius: 5,
    elevation: 5,
    shadowOpacity: 0.2,
    shadowOffset: {width: 0, height: 3}
  }),
  message: {
    fontSize: responsiveFont(16),
    fontFamily: Fonts.openSans,
    marginVertical: responsiveHeight(10),
    textAlign: 'left'
  }
})

export default ModalUploadProfile
