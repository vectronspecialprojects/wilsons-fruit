import React, {useRef} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {RNCamera} from 'react-native-camera'
import ScanArea from './components/ScanArea'
import RouteKey from '../../navigation/RouteKey'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import {useSelector} from 'react-redux'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Fonts from '../../Themes/Fonts'

function CameraScreen({navigation}) {
  const isDetected = useRef(false)
  const appFlags = useSelector(state => state.app.appFlags)
  const domain = appFlags?.app_default_website
    ? ''
    : appFlags?.app_default_website.split('.').slice(1).join('.')

  function handleScanCode(code) {
    if (code.includes('yourorder.io') || code.includes(domain)) {
      navigation.replace(RouteKey.YourorderScreen, {
        params: {
          qrcodeUri: code
        }
      })
    } else {
      isDetected.current = false
      Alert.alert(localize('camera.errTitle'), localize('camera.errMessage'), [{text: localize('okay')}])
    }
  }

  return (
    <View style={styles.container}>
      <View style={{flex: 1}}>
        <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera to scan a QR Code.',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          captureAudio={false}
          onBarCodeRead={data => {
            if (!isDetected.current) {
              isDetected.current = true
              handleScanCode(data.data)
            }
          }}
        >
          <View style={styles.textContainer}>
            <Text style={styles.title}>{localize('cameraScreen.title')}</Text>
            <Text style={{color: '#fff'}}>{localize('cameraScreen.description')}</Text>
          </View>
          <ScanArea />
        </RNCamera>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  preview: {
    flex: 1
  },
  textContainer: {
    top: responsiveHeight(15),
    left: responsiveHeight(0),
    position: 'absolute',
    zIndex: 9999,
    paddingHorizontal: responsiveWidth(20)
  },
  title: {
    fontSize: responsiveFont(20),
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(5),
    color: '#fff'
  }
})

export default CameraScreen
