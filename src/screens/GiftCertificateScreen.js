import React, {useState, useCallback, useMemo} from 'react'
import {View, FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {wait} from '../components/UtilityFunctions'
import MessageBoxPopup from './../components/MessageBoxPopup'
import Colors from '../Themes/Colors'
import CardItem from './../components/CardItem'
import CardDetail from './../components/CardDetail'
import {Card} from '../modals/modals'
import RouteKey from '../navigation/RouteKey'
import Toast from '../components/Toast'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {isTrue} from '../utilities/utils'
import ButtonVenue from '../components/ButtonVenue'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'
import {useFocusEffect} from '@react-navigation/native'

const GiftCertificateScreen = props => {
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const selectedId = useSelector(state => state.infoServices.preferredVenueId)
  const [selectedGift, setSelectedGift] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [comment, setComment] = useState('')
  const giftCertificate = useSelector(state => state.infoServices.giftCertificate)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(5))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  const isGiftExpanded = giftId => {
    if (selectedGift === giftId) {
      setIsOpen(false)
      setSelectedGift(null)
    } else {
      setIsOpen(true)
      setSelectedGift(giftId)
    }
  }

  const cancelPopup = () => {
    setIsPopupVisible(false)
    setComment('')
  }

  const submitEnquiry = async () => {
    setIsPopupVisible(false)
    try {
      const rest = await dispatch(infoServicesActions.submitEnquiry(selectedGift, popupHeader, comment))
      Toast.success(rest)
    } catch (err) {
      wait(400).then(() =>
        Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
      )
    }
    setComment('')
  }

  useFocusEffect(
    useCallback(() => {
      dispatch(setGlobalIndicatorVisibility(true))
      setIsOpen(false)
      setSelectedGift(null)
      loadContent()
    }, [dispatch, loadContent])
  )

  const switchFavorite = async (listingId, favorite) => {
    try {
      const rest = await dispatch(infoServicesActions.switchFavorite(listingId, favorite))
      dispatch(setGlobalIndicatorVisibility(true))
      loadContent()
      Toast.success(rest)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
  }

  const popupHeader = useMemo(() => {
    if (giftCertificate === undefined || !selectedGift) {
      return ''
    }
    if (giftCertificate !== undefined && selectedGift) {
      return giftCertificate?.filter(data => data.listing.id === selectedGift)[0].listing.heading
    }
  }, [giftCertificate, selectedGift])

  const showData = useMemo(() => {
    if (giftCertificate === undefined) {
      return []
    }
    if (giftCertificate !== undefined) {
      if (selectedId === 0) {
        return giftCertificate?.sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
      }
      return giftCertificate
        ?.filter(data => data.listing.venue.id === 0 || data.listing.venue.id === selectedId)
        .sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
    }
  }, [giftCertificate, selectedId])

  const renderItem = itemData => {
    const listing = itemData.item.listing
    const cardDetail = new Card(listing.heading, listing.desc_short, 0, 0, 0, listing.image_banner)
    return (
      <View style={{marginBottom: responsiveHeight(5)}}>
        <CardItem
          showDetail={!(selectedGift === listing.id && isOpen)}
          cardDetail={cardDetail}
          titleStyle={{color: Colors().heroText}}
          onPress={isGiftExpanded.bind(this, listing.id)}
        />
        {selectedGift === listing.id && isOpen && (
          <CardDetail
            title={listing.heading}
            titleStyle={{color: Colors().backgroundText}}
            data={listing}
            html={listing.desc_long}
            favorite={listing.favorite}
            onFavoritePress={switchFavorite.bind(this, listing.id, listing.favorite)}
            onChatPress={() => setIsPopupVisible(!isPopupVisible)}
            products={listing.products}
            allowBooking={isTrue(listing?.extra_settings?.add_booking) && listing?.products?.length === 1}
            allowChat={isTrue(listing?.extra_settings?.add_enquiry)}
            handleCartPress={() =>
              props.navigation.navigate(RouteKey.BuyGiftCertificateScreen, {
                listing: listing
              })
            }
          />
        )}
      </View>
    )
  }

  return (
    <ScreenContainer>
      <ButtonVenue />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={
          <ListEmpty message={'No gift giftCertificates found, please check again later.'} />
        }
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />
      <MessageBoxPopup
        isVisible={isPopupVisible}
        header={popupHeader}
        onCancelPress={cancelPopup}
        onOkPress={submitEnquiry}
        onChangeText={text => setComment(text)}
        value={comment}
      />
    </ScreenContainer>
  )
}

export default GiftCertificateScreen
