import React, {Component} from 'react'
import {View, StyleSheet, Modal} from 'react-native'
import Emitter from '../utilities/Emitter'
import {NegativeButton, PositiveButton} from './ButtonView'
import Metrics, {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {DialogText} from './Text'
import {DialogComponent} from './ComponentContainer'

export default class Alert extends Component {
  constructor() {
    super()
    this.state = {
      title: '',
      message: '',
      isVisible: false,
      buttons: [],
      messageStyle: {},
      titleStyle: {}
    }
  }

  /**
   * Show Alert text box
   * @param title title of alert text box
   * @param message message of alert text box
   * @param buttons of type {title: string, primary: bool, onPress: func}
   * @param messageStyle
   * @param titleStyle
   */
  static alert(title, message, buttons = null, messageStyle, titleStyle) {
    let actions = buttons
    if (actions === null) {
      actions = [
        {
          text: 'OK',
          primary: true,
          onPress: () => {}
        }
      ]
    }
    Emitter.emit('SHOW_ALERT', {title, message, buttons: actions, messageStyle, titleStyle})
  }

  static cancel() {
    Emitter.emit('HIDE_ALERT')
  }

  componentDidMount() {
    Emitter.on('SHOW_ALERT', data => {
      this.setState({
        ...data,
        isVisible: true
      })
    })

    Emitter.on('HIDE_ALERT', data => {
      this.setState({
        isVisible: false,
        messageStyle: {},
        titleStyle: {}
      })
    })
  }

  componentWillUnmount() {
    Emitter.rm('SHOW_ALERT')
  }

  _hide = () => {
    requestAnimationFrame(() => {
      this.setState({
        isVisible: false,
        messageStyle: {},
        titleStyle: {}
      })
    })
  }

  renderActionButton = (action, index) => {
    let Button
    switch (action.type) {
      case 'negative':
        Button = NegativeButton
        break
      case 'positive':
        Button = PositiveButton
        break
      default:
        Button = PositiveButton
        break
    }
    return (
      <Button
        style={[styles.button, action.style]}
        titleStyle={action.textStyle}
        key={index.toString()}
        title={action.text}
        onPress={() => {
          this._hide()
          action.onPress?.()
        }}
      />
    )
  }

  render() {
    const {isVisible, title, message, buttons, messageStyle, titleStyle} = this.state
    return (
      <Modal visible={isVisible} transparent={true} onRequestClose={() => {}}>
        <View style={styles.backdropContainer}>
          <DialogComponent style={styles.container}>
            <View style={styles.messageWrapper}>
              {!!title && <DialogText style={[styles.title, titleStyle]}>{title}</DialogText>}
              <DialogText style={[styles.message, messageStyle]}>{message}</DialogText>
            </View>
            <View style={styles.actions}>{buttons.map(this.renderActionButton)}</View>
          </DialogComponent>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    width: '90%',
    alignItems: 'center',
    overflow: 'hidden',
    paddingBottom: responsiveHeight(15)
  },
  message: {
    marginTop: 5,
    fontSize: responsiveFont(14),
    textAlign: 'center',
    fontFamily: Fonts.regular
  },
  messageWrapper: {
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: Metrics.small,
    paddingTop: responsiveHeight(20)
  },
  actions: {
    marginTop: responsiveHeight(30),
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(10)
  },
  primary: {},
  title: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.bold
  },
  button: {
    flex: 1,
    marginHorizontal: 5
  },
  backdropContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)'
  }
})
