import React, {useEffect, useState, useCallback} from 'react'
import {FlatList} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as infoServicesActions from '../../store/actions/infoServices'
import SurveyItem from './components/SurveyItem'
import RouteKey from '../../navigation/RouteKey'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import ScreenContainer from '../../components/ScreenContainer'
import ListEmpty from '../../components/ListEmpty'
import CustomRefreshControl from '../../components/CustomRefreshControl'

const SurveysScreen = props => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const listSurvey = useSelector(state => state.infoServices.surveys)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchSurveys())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function renderItem({item, index}) {
    return (
      <SurveyItem
        data={item}
        onPress={() => {
          props.navigation.navigate(RouteKey.SurveyDetailsScreen, {
            surveyId: item.id,
            surveyName: item.title,
            refreshList: () => loadContent()
          })
        }}
      />
    )
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={title} />
      <FlatList
        data={listSurvey}
        renderItem={renderItem}
        ListEmptyComponent={<ListEmpty message={'No survey found, please check again later.'} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
        keyExtractor={(item, index) => index.toString()}
      />
    </ScreenContainer>
  )
}

export default SurveysScreen
