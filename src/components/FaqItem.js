import React, {useState} from 'react'
import {View, StyleSheet} from 'react-native'
import Html from '../components/Html'
import {onLinkPress, TouchableCmp} from './UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Fonts from '../Themes/Fonts'
import ComponentContainer from './ComponentContainer'
import {CardText} from './Text'

const FaqItem = ({title, content}) => {
  const [show, setShow] = useState(false)
  return (
    <ComponentContainer style={styles.container}>
      <TouchableCmp activeOpacity={0.6} onPress={() => setShow(!show)}>
        <View style={styles.titleContainer}>
          <CardText style={styles.title}>{title}</CardText>
          <AntDesign name={show ? 'down' : 'right'} size={22} color={Colors().cardText} />
        </View>
      </TouchableCmp>
      {show && (
        <View style={{marginTop: responsiveHeight(5)}}>
          <Html html={`<div>${content}</div>`} textAlign={'left'} onLinkPress={onLinkPress} />
        </View>
      )}
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(18),
    padding: responsiveWidth(15),
    borderRadius: responsiveWidth(14),
    ...shadow
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: Fonts.openSansBold,
    textAlign: 'left',
    flex: 1,
    fontSize: responsiveFont(15),
    textDecorationLine: null
  }
})

export default FaqItem
