import React, {useMemo, useState} from 'react'
import {StyleSheet, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {changePhone} from '../../store/actions/authServices'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'
import SubHeaderBar from '../../components/SubHeaderBar'
import TextInputView from '../../components/TextInputView'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import ScreenContainer from '../../components/ScreenContainer'

function ChangePhoneScreen(props) {
  const [newPhone, setNewPhone] = useState('')
  const [confirmPhone, setConfirmPhone] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  async function handleChangeEmail() {
    try {
      if (validateData()) {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(changePhone(newPhone, password))
        props.navigation.pop()
        Toast.success(localize('changePhone.messageSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function validateData() {
    if (newPhone !== confirmPhone) {
      Toast.success(localize('changePhone.confirmPhoneNotMatch'))
      return false
    }
    return true
  }

  const disableButton = useMemo(() => {
    return !(newPhone && confirmPhone && password)
  }, [newPhone, confirmPhone, password])

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('changePhone.title')} />
      <ScrollView style={styles.container} bounces={false}>
        <TextInputView
          floatTitle={localize('changePhone.newPhone')}
          value={newPhone}
          onChangeText={setNewPhone}
          maxLength={10}
          keyboardType={'numeric'}
        />
        <TextInputView
          floatTitle={localize('changePhone.confirmNewPhone')}
          value={confirmPhone}
          onChangeText={setConfirmPhone}
          maxLength={10}
          keyboardType={'numeric'}
        />
        <TextInputView
          floatTitle={localize('changePhone.password')}
          secureTextEntry={true}
          value={password}
          onChangeText={setPassword}
        />
        <PositiveButton
          title={localize('changePhone.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangeEmail()}
        />
        <NegativeButton
          title={localize('changePhone.cancel')}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20)
  }
})
export default ChangePhoneScreen
