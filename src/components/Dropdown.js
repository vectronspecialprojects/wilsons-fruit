import React, {useState, useEffect, useRef} from 'react'
import {StyleSheet, View} from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import {responsiveHeight} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import TextInputView from './TextInputView'

const Dropdown = props => {
  const {
    style,
    placeholder,
    disabled = false,
    onInputChange,
    id,
    items,
    initValue,
    titleStyle,
    containerStyle,
    backgroundDisabled
  } = props
  const [value, setValue] = useState(initValue)
  const pickerRef = useRef()

  useEffect(() => {
    if (!value && initValue) {
      setValue(initValue)
    }
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [initValue])

  const valueChangeHandler = data => {
    let isValid = true
    if (props.required && !data) {
      isValid = false
    }
    setValue(data)
    onInputChange?.(id, data, isValid)
  }

  return (
    <View style={[styles.formControl, containerStyle]}>
      <TextInputView
        floatTitle={props?.label}
        editable={false}
        onPress={() => {
          pickerRef.current?.togglePicker(true)
        }}
        value={value}
        placeholder={placeholder}
        titleStyle={titleStyle}
        style={style}
        backgroundDisabled={backgroundDisabled}
      />
      <View style={{height: 0, overflow: 'hidden'}}>
        <RNPickerSelect
          ref={pickerRef}
          onValueChange={valueChangeHandler}
          items={items}
          value={value}
          disabled={disabled}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  formControl: {
    width: '100%'
  },
  label: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(4)
  }
})

export default Dropdown
