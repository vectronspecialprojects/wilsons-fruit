import React, {useEffect, useState} from 'react'
import {View, StyleSheet} from 'react-native'
import Swiper from 'react-native-swiper'
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import FastImage from 'react-native-fast-image'
import Fonts from '../../../Themes/Fonts'
import ButtonView from '../../../components/ButtonView'
import Colors from '../../../Themes/Colors'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import MenuItem from './MenuItem'
import {getCommunityPoint} from '../../../utilities/ApiManage'
import {useSelector} from 'react-redux'
import {CardText, Link} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'

function CommunityDataComponent({profile, communityImpact, pledge, item, ifRoute}) {
  const pondHopper = useSelector(state => state.app.pondHopper)
  const [pointData, sePointData] = useState()

  useEffect(() => {
    try {
      if (pondHopper.url) {
        getCommunityPoint(
          pondHopper.url,
          pondHopper.uniqueKey,
          profile?.member?.bepoz_account_number,
          profile?.member?.current_preferred_venue_full?.bepoz_venue_id
        ).then(res => {
          sePointData(res)
        })
      }
    } catch (e) {
      console.log(e)
    }
  }, [pondHopper, profile])

  return (
    <View style={{marginBottom: responsiveHeight(15), marginHorizontal: responsiveWidth(15)}}>
      <MenuItem
        title={'Community impact'}
        subTitle={'Pledge & nominate'}
        style={{marginTop: responsiveHeight(15), marginHorizontal: 0}}
        color={item.icon_color}
        dataTitle={item.page_name}
        value={`${pointData?.pointsAvailable || 0} Community Points ready to pledge`}
        iconName={item.icon}
        iconImage={item.image_icon}
        valueRight={false}
        onPress={() => ifRoute(item.id)}
        iconSelector={item.icon_selector}
      />
      <ComponentContainer style={styles.carouselContainer}>
        <Swiper loop={true} style={{height: responsiveHeight(130)}} autoplay={true} showsPagination={false}>
          {communityImpact?.community_impact_gallery?.map((item, index) => {
            return (
              <FastImage
                key={index.toString()}
                source={{uri: item.url}}
                style={{width: '100%', height: responsiveHeight(130)}}
              />
            )
          })}
        </Swiper>
        <View style={styles.totalView}>
          <Link style={styles.point}>{pointData?.totalPledgedToDate || '$0.00'}</Link>
          <CardText style={styles.totalText}>{communityImpact?.community_impact_content_text}</CardText>
          <ButtonView
            title={'Read Now'}
            style={[styles.buttonReadNow, {borderColor: Colors().heroFill}]}
            titleStyle={{color: Colors().heroFill}}
            onPress={() => {
              navigate(RouteKey.PondHoppersScreen, {
                action: communityImpact?.community_impact_button_option.toUpperCase(),
                title: communityImpact?.community_impact_button_option.toLowerCase()
              })
            }}
          />
        </View>
      </ComponentContainer>
    </View>
  )
}

const styles = StyleSheet.create({
  carouselContainer: {
    ...shadow
  },
  buttonReadNow: {
    width: responsiveWidth(120),
    height: responsiveHeight(40),
    backgroundColor: 'transparent',
    borderWidth: 1,
    marginTop: responsiveHeight(15)
  },
  totalView: {
    paddingVertical: responsiveHeight(15),
    paddingHorizontal: responsiveWidth(10)
  },
  totalText: {
    fontSize: responsiveFont(12),
    fontFamily: Fonts.regular
  },
  point: {
    fontFamily: Fonts.bold,
    fontSize: responsiveFont(18)
  }
})
export default CommunityDataComponent
