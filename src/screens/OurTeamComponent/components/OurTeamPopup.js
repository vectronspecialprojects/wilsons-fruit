import React, {useEffect, useState} from 'react'
import {View, StyleSheet, Image} from 'react-native'
import Modal from 'react-native-modal'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import fonts from '../../../Themes/Fonts'
import {onLinkPress} from '../../../components/UtilityFunctions'
import Html from '../../../components/Html'
import ButtonView from '../../../components/ButtonView'
import {CardText, HeroText} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'

function OurTeamPopup({isVisible, item, onPress}) {
  const [data, setData] = useState(item)

  useEffect(() => {
    if (item) {
      setData(item)
    }
  }, [item])

  return (
    <Modal isVisible={isVisible} animationIn="swing" animationOut="zoomOut">
      <ComponentContainer style={styles.popupContainer}>
        <View style={{width: '100%', aspectRatio: 1}}>
          <Image style={styles.image} source={{uri: data?.image_square}} />
        </View>
        <View style={{padding: responsiveWidth(20)}}>
          <CardText style={styles.staffName}>{data?.name}</CardText>
          <HeroText style={styles.venueName}>{data?.venue_name}</HeroText>
          <Html html={`<div>${data?.desc_long}</div>`} textAlign={'center'} onLinkPress={onLinkPress} />
        </View>
        <ButtonView onPress={onPress} title={'Close'} />
      </ComponentContainer>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  popupContainer: {
    alignSelf: 'center',
    width: deviceWidth() * 0.7,
    borderRadius: responsiveWidth(15),
    overflow: 'hidden'
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover'
  },
  staffName: {
    fontFamily: fonts.openSansBold,
    fontSize: responsiveFont(16),
    textAlign: 'center'
  },
  venueName: {
    fontFamily: fonts.openSansBold,
    fontSize: responsiveFont(14),
    textAlign: 'center',
    marginTop: responsiveHeight(10)
  }
})

export default OurTeamPopup
