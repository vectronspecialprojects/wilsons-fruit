import React, {useState, useCallback, useMemo} from 'react'
import {FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import TabsBar from './../components/TabsBar'
import CardItem from './../components/CardItem'
import RouteKey from '../navigation/RouteKey'
import {Card} from '../modals/modals'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ButtonVenue from '../components/ButtonVenue'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'
import {useFocusEffect} from '@react-navigation/native'
import Colors from '../Themes/Colors'

const WhatsonScreen = ({navigation, route}) => {
  const pages = route?.params?.params?.pages
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedPage, setSelectedPage] = useState(+pages[0]?.listing_type_id || 2)
  const selectedId = useSelector(state => state.infoServices.preferredVenueId)
  const specialEvents = useSelector(state => state.infoServices.specialEvents)
  const regularEvents = useSelector(state => state.infoServices.regularEvents)
  const internetState = useSelector(state => state.app.internetState)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(1))
      await dispatch(infoServicesActions.fetctListings(2))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useFocusEffect(
    useCallback(() => {
      if (internetState) {
        dispatch(setGlobalIndicatorVisibility(true))
        loadContent()
      }
    }, [internetState, loadContent, dispatch])
  )

  const tabsHandler = useCallback(
    id => {
      if (selectedPage !== id) {
        setSelectedPage(+id)
      }
    },
    [selectedPage]
  )

  const showData = useMemo(() => {
    if (regularEvents !== undefined && specialEvents !== undefined) {
      if (selectedPage === 2) {
        if (selectedId === 0) {
          return regularEvents?.sort((a, b) => {
            return a?.listing?.display_order - b?.listing?.display_order
          })
        }
        return regularEvents
          ?.filter(data => data?.listing?.venue?.id === 0 || data?.listing?.venue?.id === selectedId)
          ?.sort((a, b) => {
            return a?.listing?.display_order - b?.listing?.display_order
          })
      }
      if (selectedId === 0) {
        return specialEvents?.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
      }
      return specialEvents
        ?.filter(data => data?.listing?.venue.id === 0 || data?.listing?.venue?.id === selectedId)
        ?.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
    }
  }, [regularEvents, specialEvents, selectedPage, selectedId])

  const renderItem = itemData => {
    const listing = itemData.item?.listing
    const cardDetail = new Card(
      listing?.heading,
      listing?.desc_short,
      listing?.payload,
      0,
      0,
      listing?.image_banner
    )
    return (
      <CardItem
        style={{marginBottom: responsiveHeight(5)}}
        titleStyle={{color: Colors().heroText}}
        cardDetail={cardDetail}
        onPress={() => {
          navigation.navigate(RouteKey.WhatsonDetailsScreen, {listing})
        }}
      />
    )
  }

  return (
    <ScreenContainer>
      <ButtonVenue />
      <TabsBar isShadow={true} pages={pages} onPress={tabsHandler} selectedPageId={selectedPage} />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={<ListEmpty message={'No Events found, please check again later.'} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              loadContent()
              setIsRefreshing(true)
            }}
          />
        }
      />
    </ScreenContainer>
  )
}

export default WhatsonScreen
