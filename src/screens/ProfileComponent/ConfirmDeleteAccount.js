import React, {useCallback, useState} from 'react'
import Modal from 'react-native-modal'
import {View, StyleSheet} from 'react-native'
import {DialogComponent} from '../../components/ComponentContainer'
import {DialogText, ErrorText} from '../../components/Text'
import {localize} from '../../locale/I18nConfig'
import Fonts from '../../Themes/Fonts'
import Metrics, {FontSizes} from '../../Themes/Metrics'
import {useDispatch, useSelector} from 'react-redux'
import TextInputView from '../../components/TextInputView'
import {NegativeButton, PositiveButton} from '../../components/ButtonView'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {confirmDeleteAccount} from '../../store/actions/authServices'
import IndicatorDialog from '../../components/IndicatorDialog'

const ConfirmDeleteAccount = ({visible, onDismiss}) => {
  const profile = useSelector(state => state.infoServices.profile)
  const [code, setCode] = useState('')
  const dispatch = useDispatch()
  const showGlobalIndicator = useSelector(state => state.app.showGlobalIndicator)
  const [error, setError] = useState('')

  const handleSubmitDeletionCode = useCallback(
    async code => {
      try {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(confirmDeleteAccount(code))
        onDismiss()
      } catch (e) {
        setError(e.message)
      } finally {
        dispatch(setGlobalIndicatorVisibility(false))
      }
    },
    [dispatch, onDismiss]
  )

  return (
    <Modal isVisible={visible} avoidKeyboard>
      <DialogComponent style={styles.container}>
        <DialogText fontSize={FontSizes.title} centered style={styles.title}>
          {localize('profile.confirmDeleteTitle')}
        </DialogText>
        <DialogText>
          {localize('profile.confirmDeleteMessage', {
            memberName: `${profile?.member?.first_name} ${profile?.member?.last_name}`
          })}
        </DialogText>
        <TextInputView
          placeholderTextInput={'Enter deletion code'}
          textInputStyle={styles.textInput}
          style={styles.textInputContainer}
          value={code}
          onChangeText={setCode}
          keyboardType={'numeric'}
        />
        {!!error && <ErrorText centered>{error}</ErrorText>}
        <View style={styles.row}>
          <NegativeButton title={'Cancel'} style={styles.button} onPress={onDismiss} />
          <PositiveButton
            title={'Confirm'}
            style={styles.button}
            onPress={() => handleSubmitDeletionCode(code)}
          />
        </View>
        {showGlobalIndicator && <IndicatorDialog />}
      </DialogComponent>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: Metrics.small,
    borderRadius: Metrics.borderRadiusLarge * 2
  },
  title: {
    marginBottom: Metrics.small,
    fontFamily: Fonts.bold
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Metrics.small
  },
  button: {
    flex: 1,
    maxWidth: '45%'
  },
  textInputContainer: {
    marginTop: Metrics.small
  },
  textInput: {
    textAlign: 'center'
  }
})

export default ConfirmDeleteAccount
