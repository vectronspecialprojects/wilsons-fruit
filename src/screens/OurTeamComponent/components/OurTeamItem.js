import React from 'react'
import {View, StyleSheet, ImageBackground} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import {CardText} from '../../../components/Text'
import ComponentContainer from '../../../components/ComponentContainer'

function OurTeamItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <ComponentContainer style={styles.container}>
        <ImageBackground source={{uri: data.image_square}} style={styles.image}>
          <View style={[styles.contentWrapper, {backgroundColor: Colors().opacity}]}>
            <CardText style={styles.title}>{data.name}</CardText>
            <CardText style={styles.content}>{data.venue_name || data.venue?.name}</CardText>
          </View>
        </ImageBackground>
      </ComponentContainer>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    borderRadius: responsiveWidth(5),
    overflow: 'hidden',
    marginTop: responsiveHeight(20),
    marginRight: responsiveWidth(20)
  },
  image: {
    width: responsiveWidth(150),
    height: responsiveWidth(150)
  },
  contentWrapper: {
    padding: responsiveWidth(8),
    flex: 1,
    justifyContent: 'space-between'
  },
  title: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.bold,
    textAlign: 'center'
  },
  content: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.regular
  }
})

export default OurTeamItem
