import RouteKey from './RouteKey'
import LoginScreen from '../screens/AuthComponent/LoginScreen'
import SignupScreen from '../screens/AuthComponent/SignupScreen'
import ForgotPasswordScreen from '../screens/AuthComponent/ForgotPasswordScreen'
import PreferredVenueScreen from '../screens/PreferredVenueComponent/PreferredVenueScreen'
import MapScreen from '../screens/MapScreen'
import ProfileScreen from '../screens/ProfileComponent/ProfileScreen'
import HistoryScreen from '../screens/HistoryScreen'
import FavoriteScreen from '../screens/FavoriteScreen'
import StartupScreen, {screenOptions as startupScreenOptions} from '../screens/StartUpComponent/StartupScreen'
import AboutScreen from '../screens/AboutScreen'
import WhatsonScreen from '../screens/WhatsonScreen'
import WhatsonDetailsScreen from '../screens/WhatsonDetailsScreen'
import ReferScreen from '../screens/ReferScreen'
import TicketsScreen from '../screens/TicketComponent/TicketsScreen'
import GiftCertificateScreen from '../screens/GiftCertificateScreen'
import SurveysScreen from '../screens/SurveyComponent/SurveysScreen'
import LocationsScreen from '../screens/LocationsScreen'
import StampcardScreen from '../screens/StampcardScreen'
import FaqScreen from '../screens/FaqScreen'
import FeedbackScreen from '../screens/FeedbackScreen'
import VouchersScreen from '../screens/VouchersScreen'
import VoucherDetailScreen from '../screens/VoucherDetailScreen'
import OffersScreen from '../screens/OffersScreen'
import EnquiriesScreen from '../screens/EnquiriesScreen'
import LegalsScreen from '../screens/LegalsScreen'
import HomeScreen, {screenOptions as homeScreenOptions} from '../screens/HomeComponent/HomeScreen'
import WebviewScreen from '../screens/WebviewScreen'
import YourorderScreen from '../screens/YourorderScreen'
import ResetPasswordScreen from '../screens/ProfileComponent/ResetPasswordScreen'
import ChangeEmailScreen from '../screens/ProfileComponent/ChangeEmailScreen'
import SurveyDetailsScreen from '../screens/SurveyComponent/SurveyDetailsScreen'
import OurTeamScreen from '../screens/OurTeamComponent/OurTeamScreen'
import FavoriteDetailScreen from '../screens/ProfileComponent/FavoriteDetailScreen'
import TicketDetailScreen from '../screens/TicketComponent/TicketDetailScreen'
import ProductListScreen from '../screens/ProductComponent/ProductListScreen'
import BuyGiftCertificateScreen from '../screens/BuyGiftCertificateScreen'
import MatchAccountScreen from '../screens/AuthComponent/MatchAccountScreen'
import MatchAccountInfo from '../screens/AuthComponent/MatchAccountInfo'
import GamingScreen from '../screens/GamingComponent/GamingScreen'
import WheelGameScreen from '../screens/GamingComponent/WheelGameScreen'
import MembershipScreen from '../screens/MembershipComponent/MembershipScreen'
import MembershipDetailsScreen from '../screens/MembershipComponent/MembershipDetailsScreen'
import ShopsScreen from '../screens/ShopsComponent/ShopsScreen'
import ScratchGameScreen from '../screens/GamingComponent/ScratchGameScreen'
import CameraScreen from '../screens/CameraComponent/CameraScreen'
import VerifyPhoneAndEmail from '../screens/AuthComponent/VerifyPhoneAndEmail'
import PondHoppersScreen from '../screens/HomeComponent/PondHoppersScreen'
import ConnectWithUsScreen from '../screens/ConnectWithUsComponent/ConnectWithUsScreen'
import ChangePhoneScreen from '../screens/ProfileComponent/ChangePhoneScreen'
import UpdateAccountScreen from '../screens/ProfileComponent/UpdateAccountScreen'
import HeaderLeftButton from '../components/HeaderLeftButton'
import React from 'react'
import UpdateRequireInfoScreen from '../screens/AuthComponent/UpdateRequireInfoScreen'

export const screenMatch = screen => {
  switch (screen) {
    case RouteKey.LoginScreen:
      return LoginScreen
    case RouteKey.ForgotPasswordScreen:
      return ForgotPasswordScreen
    case RouteKey.SignupScreen:
      return SignupScreen
    case RouteKey.PreferredVenueScreen:
      return PreferredVenueScreen
    case RouteKey.MapScreen:
      return MapScreen
    case RouteKey.ProfileScreen:
      return ProfileScreen
    case RouteKey.HistoryScreen:
      return HistoryScreen
    case RouteKey.FavoriteScreen:
      return FavoriteScreen
    case RouteKey.StartupScreen:
      return StartupScreen
    case RouteKey.AboutScreen:
      return AboutScreen
    case RouteKey.WhatsonScreen:
      return WhatsonScreen
    case RouteKey.WhatsonDetailsScreen:
      return WhatsonDetailsScreen
    case RouteKey.ReferScreen:
      return ReferScreen
    case RouteKey.TicketsScreen:
      return TicketsScreen
    case RouteKey.GiftCertificateScreen:
      return GiftCertificateScreen
    case RouteKey.SurveysScreen:
      return SurveysScreen
    case RouteKey.LocationsScreen:
      return LocationsScreen
    case RouteKey.StampcardScreen:
      return StampcardScreen
    case RouteKey.FaqScreen:
      return FaqScreen
    case RouteKey.FeedbackScreen:
      return FeedbackScreen
    case RouteKey.VouchersScreen:
      return VouchersScreen
    case RouteKey.VoucherDetailScreen:
      return VoucherDetailScreen
    case RouteKey.OffersScreen:
      return OffersScreen
    case RouteKey.EnquiriesScreen:
      return EnquiriesScreen
    case RouteKey.LegalsScreen:
      return LegalsScreen
    case RouteKey.HomeScreen:
      return HomeScreen
    case RouteKey.WebviewScreen:
      return WebviewScreen
    case RouteKey.YourorderScreen:
      return YourorderScreen
    case RouteKey.ResetPasswordScreen:
      return ResetPasswordScreen
    case RouteKey.ChangeEmailScreen:
      return ChangeEmailScreen
    case RouteKey.SurveyDetailsScreen:
      return SurveyDetailsScreen
    case RouteKey.OurTeamScreen:
      return OurTeamScreen
    case RouteKey.FavoriteDetailScreen:
      return FavoriteDetailScreen
    case RouteKey.TicketDetailScreen:
      return TicketDetailScreen
    case RouteKey.ProductListScreen:
      return ProductListScreen
    case RouteKey.BuyGiftCertificateScreen:
      return BuyGiftCertificateScreen
    case RouteKey.MatchAccountScreen:
      return MatchAccountScreen
    case RouteKey.MatchAccountInfo:
      return MatchAccountInfo
    case RouteKey.GamingScreen:
      return GamingScreen
    case RouteKey.WheelGameScreen:
      return WheelGameScreen
    case RouteKey.MembershipScreen:
      return MembershipScreen
    case RouteKey.MembershipDetailsScreen:
      return MembershipDetailsScreen
    case RouteKey.ShopsScreen:
      return ShopsScreen
    case RouteKey.ScratchGameScreen:
      return ScratchGameScreen
    case RouteKey.CameraScreen:
      return CameraScreen
    case RouteKey.VerifyPhoneAndEmail:
      return VerifyPhoneAndEmail
    case RouteKey.PondHoppersScreen:
    case RouteKey.CommunityPointScreen:
      return PondHoppersScreen
    case RouteKey.ConnectWithUsScreen:
      return ConnectWithUsScreen
    case RouteKey.ChangePhoneScreen:
      return ChangePhoneScreen
    case RouteKey.UpdateAccountScreen:
      return UpdateAccountScreen
    case RouteKey.UpdateRequireInfoScreen:
      return UpdateRequireInfoScreen
  }
}

const defaultScreenOptions = navData => ({
  headerLeft: () => <HeaderLeftButton navData={navData} />
})

export const optionsMatch = screen => {
  switch (screen) {
    case RouteKey.StartupScreen:
      return startupScreenOptions
    case RouteKey.HomeScreen:
      return homeScreenOptions
    default:
      return defaultScreenOptions
  }
}
